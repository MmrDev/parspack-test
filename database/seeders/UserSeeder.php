<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'first_name' => 'محمدرضا',
                'last_name' => 'روستانا',
                'email' => 'mr.roustana@gmail.com',
                'mobile' => '9384275307',
                'username' => 'MmrDev',
                'password' => bcrypt('123123'),
                'type' => 'member',
            ]
        ];
        foreach ($data as $item) {
            User::create($item);
        }
    }
}
