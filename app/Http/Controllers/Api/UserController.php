<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{


    public function profile()
    {
        return $this->successResponse(200, new UserResource(Auth::user()), 200);
    }
}
