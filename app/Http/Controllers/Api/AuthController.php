<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    private $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function login(LoginRequest $request)
    {
        $data = $this->repository->checkMail($request->validated());
        if ($data) {
            $userInstance = $data['userInstance'];
            if (Auth::attempt($data['credentials'])) {
                // login user
                Auth::login($userInstance);
                // get token by type
               $token = $this->repository->createToken($userInstance);

                return $this->successResponse('user_login', [
                    'userInstance' => new UserResource($userInstance),
                    'token' => $token->accessToken
                ], 200);
            }
            return $this->errorResponse(400, __('errors.wrong_username_password'), 400);
        }
        return $this->errorResponse(400, __('errors.user_not_exists'), 400);
    }


    public function register(RegisterRequest $request)
    {
       $userInstance = $this->repository->create($request->validated());
        if ($userInstance instanceof User) {
            return $this->successResponse(200, __('messages.register_successfully'), 200);
        }
        return $this->errorResponse(400, __('errors.try_again'), 400);
    }

}
