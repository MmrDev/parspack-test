<?php

namespace App\Http\Controllers\Os;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProcessController extends Controller
{
    public function getOsProcess()
    {
        $command = Request()->has('option') ? 'ps ' . Request()->get('option') : 'ps ' . '-aux';
        exec($command, $output);

        $result = [];
        foreach ($output as $item) {
            $result[] = $item;
        }
        return $this->successResponse(200, $result, 200);
    }


}
