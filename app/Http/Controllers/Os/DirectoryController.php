<?php

namespace App\Http\Controllers\Os;

use App\Http\Controllers\Controller;
use App\Http\Requests\DirRequest;
use App\Http\Requests\FileRequest;
use App\Repositories\File\FileRepositoryInterface;
use Illuminate\Http\Request;

class DirectoryController extends Controller
{
    protected $repository;

    public function __construct(FileRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function createDir(DirRequest $request)
    {
        $address = '/opt/myprogram/' . auth()->user()->username . '/' . $request->input('name');
        $dirInstance = $this->repository->createDir($address);
        if ($dirInstance) {
            return $this->successResponse(200,
                'directory created successfully in ' . $address,
                200);
        }
        return $this->successResponse(200,
            'directory already exists in ' . $address,
            200);
    }


    public function indexDir()
    {
        $address = '/opt/myprogram/' . auth()->user()->username . '/';
        $directories = $this->repository->getDirList($address);
        if ($directories){
            return $this->successResponse(200, $directories, 200);
        }
        return $this->errorResponse(400,'no such file or directory', 400);
    }

    public function createFile(FileRequest $request)
    {
        $address = '/opt/myprogram/' . auth()->user()->username . '/' . $request->input('name') . '.' . $request->input('type');
        $fileInstance = $this->repository->createFile($address);
        if ($fileInstance === 'exist'){
            return $this->successResponse(200,
                'file already exists in ' . $address,
                200);
        }elseif ($fileInstance){
            return $this->successResponse(200,
                'file created successfully in ' . $address,
                200);
        }else{
            return $this->successResponse(400,
                'try again',
                400);
        }
    }

    public function indexFile()
    {
        $address = '/opt/myprogram/' . auth()->user()->username . '/';
        $files = $this->repository->getFileList($address);
        if ($files){
             return $this->successResponse(200, $files, 200);
        }else{
             return $this->errorResponse(400,'no such file or directory', 400);
        }
    }

}
