<?php


namespace App\Repositories\User;


use App\Models\User;

class UserRepository implements UserRepositoryInterface
{
    public function checkMail(array $data)
    {
        $find1 = strpos($data['username'], '@');
        if ($find1) {
            if (strpos($data['username'], '.')) {
                $userInstance = User::where('email', $data['username'])->first();
                if ($userInstance instanceof User) {
                    return [
                        'credentials' => [
                            'email' => $userInstance['email'],
                            'password' => $data['password'],
                        ],
                        'userInstance' => $userInstance
                    ];
                }
                return false;
            };
        }
        $userInstance = User::where('username', $data['username'])->first();
        if ($userInstance instanceof User) {
            return [
                'credentials' => [
                    'email' => $userInstance['email'],
                    'password' => $data['password'],
                ],
                'userInstance' => $userInstance
            ];
        }
        return false;
    }

    public function create(array $data)
    {
        $data['password'] = bcrypt($data['password']);
        return User::create($data);
    }

    public function createToken(User $userInstance)
    {
        switch ($userInstance['type']) {
            case 'member':
                return auth()->user()->createToken('full user'
                    , ['create',
                        'update',
                        'show',
                        'delete']);
            case 'guest':
                return auth()->user()->createToken('guest user'
                    , [
                        'update',
                        'show',
                        'delete'
                    ]);
            default:
                return false;
        }
    }
}
