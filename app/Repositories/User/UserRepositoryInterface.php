<?php

namespace App\Repositories\User;

use App\Models\User;

interface UserRepositoryInterface
{

    public function checkMail(array $data);

    public function create(array $data);

    public function createToken(User $userInstance);

}
