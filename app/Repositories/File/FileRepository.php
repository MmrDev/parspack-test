<?php


namespace App\Repositories\File;


use Illuminate\Support\Facades\File;

class FileRepository implements FileRepositoryInterface
{

    public function createDir(string $address)
    {
        if (!File::isDirectory($address)) {
            return File::makeDirectory($address, 0777, true, true);
        }
        return false;
    }

    public function getDirList(string $address)
    {
        try {
            return File::directories($address);
        }catch (\Exception $e){
            return false;
        }
    }

    public function createFile(string $address)
    {
        if (File::isFile($address)){
            return 'exist';
        }
        try {
            File::put($address, '');
            return true;
        }catch (\Exception $e){
            return false;
        }

    }

    public function getFileList(string $address)
    {
        try {
            $files = File::files($address);
            $result = [];
            foreach ($files as $file) {
                $result[] = $file->getPathname();
            }
            return $result;
        }catch (\Exception $e){
            return false;
        }
    }
}
