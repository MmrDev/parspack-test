<?php


namespace App\Repositories\File;


interface FileRepositoryInterface
{

    public function createDir(string $address);

    public function getDirList(string $address);

    public function createFile(string $address);

    public function getFileList(string $address);

}
