<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Os\DirectoryController;
use App\Http\Controllers\Os\ProcessController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'user'], function () {
    // without Token
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);

    Route::group(['middleware' => ['auth:api']], function () {
        // with Token
        Route::get('logout', function () {
            Auth::user()->token()->revoke();
        });
        Route::get('profile', [UserController::class, 'profile'])->middleware('scopes:show');
    });
});

Route::group(['prefix' => 'os', 'middleware' => ['auth:api']], function () {

    Route::group(['prefix' => 'process'], function () {
        // with Token
        Route::get('process', [ProcessController::class, 'getOsProcess']);
    });

    Route::group(['prefix' => 'directory'], function () {
        // with Token
        Route::post('', [DirectoryController::class, 'createDir']);
        Route::get('', [DirectoryController::class, 'indexDir']);
    });

    Route::group(['prefix' => 'file'], function () {
        // with Token
        Route::post('', [DirectoryController::class, 'createFile']);
        Route::get('', [DirectoryController::class, 'indexFile']);
    });

});



