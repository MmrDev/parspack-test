<?php

return [


    /*
    |--------------------------------------------------------------------------
    | errors Messages
    |--------------------------------------------------------------------------
    */

    '400' => 'Something went wrong, You got bad request',
    'wrong_username_password' => 'Username or Password is wrong, please check and try again',
    'user_not_exists' => 'This account does not exists',
    'try_again' => 'try again later',
    'noData' => 'no data detected, try later'

];
